Etomica source code is a cloned version of the current development trunk obtained from https://github.com/etomica/etomica.

Earlier compilation of Etomica (compiled around Summer 2015) which was used for studying demixing in hardsphere mixtures is provided as an executable jar.

My work focused mainly on setting up and running the simulations for hardsphere mixtures to get virial coefficients. 

Core code was written by Dr. Andrew Schultz and Dr. David Kofke, I modified simulation files to make it suitable for my intended system.

Original file is in the repo https://github.com/etomica/etomica/blob/master/etomica-apps/src/main/java/etomica/virial/simulations/VirialHSMixture.java