BEGIN {
   if (col==0) col=2;
   if (skip==0) skip=1;
   if (block==0) block=1;
   countdown=block*skip;

}

{
   blockSum[NR%skip]+=$col;
   #c[NR%skip]+=$col*$col;
   countdown--;
   if (countdown==0) {
     for (i=0; i<skip; i++) {
       blockSum[i] /= block;
       a[i]+=blockSum[i];
       b[i]+=blockSum[i]*blockSum[i];
       if (!(i in firstBlock)) {
         firstBlock[i]=blockSum[i];
       }
       if (i in lastBlock) {
         c[i]+=blockSum[i]*lastBlock[i];
       }
       n[i]++;
       lastBlock[i]=blockSum[i];
       blockSum[i]=0;
     }
     countdown=block*skip;
  }
}

END {
   for (i in a) {
      if (n[i] == 1) {
        printf "%14.8e  %14d  %8d\n", a[i]/n[i], 0, 0;
        continue;
      }
      b[i]=b[i]*n[i];
      a2[i]=a[i]*a[i];
      s=(b[i]-a2[i])/(n[i]*(n[i]-1));
      bc=(((-2*a[i] + firstBlock[i] + lastBlock[i])*a[i]/n[i] + c[i])/(n[i]-1)+a2[i]/(n[i]*n[i]))/s;
      printf "%15.9e  %15.9e  %8.2e  %5.3f\n", a[i]/n[i], (s*block)^0.5, (s/n[i])^0.5, bc;
   }
}
